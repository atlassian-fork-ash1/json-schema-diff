import {JsonSchema} from 'json-schema-spec-types';
import {invokeDiffAndExpectToFail} from '../support/invoke-diff';
import {DiffTestCase, registerDiffTestCases} from '../support/register-diff-test-cases';

describe('diff-schemas references', () => {
    const testCases: DiffTestCase[] = [
        {
            description: 'schema with string property reference to schema with inline number property',
            examples: [{}, {id: 1}, {id: 'foo'}, {id: true}, 'foo'],
            input: {
                a: {
                    definitions: {
                        basic_type: {
                            type: 'string'
                        }
                    },
                    properties: {
                        id: {
                            $ref: '#/definitions/basic_type'
                        }
                    },
                    type: 'object'
                },
                b: {
                    properties: {
                        id: {
                            type: 'number'
                        }
                    },
                    type: 'object'
                }
            },
            output: {
                added: {
                    properties: {
                        id: {type: 'number'}
                    },
                    required: ['id'],
                    type: 'object'
                },
                removed: {
                    properties: {
                        id: {type: 'string'}
                    },
                    required: ['id'],
                    type: 'object'
                }
            }
        },
        {
            description: 'schema with nested string reference to schema with nested number reference',
            examples: [{}, {id: {}}, {id: {content: 1}}, {id: {content: 'foo'}}, {id: {content: true}}, 'foo'],
            input: {
                a: {
                    definitions: {
                        basic_object: {
                            properties: {
                                content: {
                                    $ref: '#/definitions/basic_type'
                                }
                            },
                            type: 'object'
                        },
                        basic_type: {
                            type: 'string'
                        }
                    },
                    properties: {
                        id: {
                            $ref: '#/definitions/basic_object'
                        }
                    },
                    type: 'object'
                },
                b: {
                    definitions: {
                        basic_object: {
                            properties: {
                                content: {
                                    $ref: '#/definitions/basic_type'
                                }
                            },
                            type: 'object'
                        },
                        basic_type: {
                            type: 'number'
                        }
                    },
                    properties: {
                        id: {
                            $ref: '#/definitions/basic_object'
                        }
                    },
                    type: 'object'
                }
            },
            output: {
                added: {
                    properties: {
                        id: {
                            properties: {
                                content: {type: 'number'}
                            },
                            required: ['content'],
                            type: 'object'
                        }
                    },
                    required: ['id'],
                    type: 'object'
                },
                removed: {
                    properties: {
                        id: {
                            properties: {
                                content: {type: 'string'}
                            },
                            required: ['content'],
                            type: 'object'
                        }
                    },
                    required: ['id'],
                    type: 'object'
                }
            }
        }
    ];

    registerDiffTestCases(testCases);

    it('should fail when schema contains circular references', async () => {
        const schemaWithCircularReferences: JsonSchema = {
            additionalProperties: {
                $ref: '#/definitions/aDefinition'
            },
            definitions: {
                aDefinition: {
                    additionalProperties: {
                        $ref: '#/definitions/aDefinition'
                    },
                    type: 'object'
                }
            },
            type: 'object'
        };

        const error = await invokeDiffAndExpectToFail(schemaWithCircularReferences, {});

        expect(error.message).toContain('Circular $ref pointer found');
    });
});
