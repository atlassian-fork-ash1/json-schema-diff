import {DiffResult} from '../api-types';
import {logSetDebug} from './common/logger';
import {dereferenceSchema} from './diff-schemas/dereference-schema';
import {parseAsJsonSet} from './diff-schemas/parse-as-json-set';
import {validateSchemas} from './diff-schemas/validate-schemas';

export const diffSchemas = async (sourceSchema: any, destinationSchema: any): Promise<DiffResult> => {
    const [dereferencedSourceSchema, dereferencedDestinationSchema] = await Promise.all([
        dereferenceSchema(sourceSchema), dereferenceSchema(destinationSchema)
    ]);

    await validateSchemas(sourceSchema, destinationSchema);

    const sourceSet = parseAsJsonSet(dereferencedSourceSchema);
    logSetDebug('sourceSet', sourceSet);
    const destinationSet = parseAsJsonSet(dereferencedDestinationSchema);
    logSetDebug('destinationSet', destinationSet);

    const intersectionOfSets = sourceSet.intersect(destinationSet);
    logSetDebug('intersectionOfSets', intersectionOfSets);
    const intersectionOfSetsComplement = intersectionOfSets.complement();
    logSetDebug('intersectionOfSetsComplement', intersectionOfSetsComplement);
    const addedToDestinationSet = intersectionOfSetsComplement.intersect(destinationSet);
    logSetDebug('addedToDestinationSet', addedToDestinationSet);
    const removedFromDestinationSet = intersectionOfSetsComplement.intersect(sourceSet);
    logSetDebug('removedFromDestinationSet', removedFromDestinationSet);

    return {
        addedJsonSchema: addedToDestinationSet.toJsonSchema(),
        additionsFound: addedToDestinationSet.type !== 'empty',
        removalsFound: removedFromDestinationSet.type !== 'empty',
        removedJsonSchema: removedFromDestinationSet.toJsonSchema()
    };
};
