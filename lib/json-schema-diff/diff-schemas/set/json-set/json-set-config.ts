import {Set} from '../set';

export interface JsonSetConfig {
    array: Set<'array'>;
    boolean: Set<'boolean'>;
    integer: Set<'integer'>;
    null: Set<'null'>;
    number: Set<'number'>;
    object: Set<'object'>;
    string: Set<'string'>;
}

const mapJsonSetConfigs = (
    a: JsonSetConfig, b: JsonSetConfig, callbackFn: <T> (typeSet1: Set<T>, typeSet2: Set<T>) => Set<T>
): JsonSetConfig => ({
    array: callbackFn(a.array, b.array),
    boolean: callbackFn(a.boolean, b.boolean),
    integer: callbackFn(a.integer, b.integer),
    null: callbackFn(a.null, b.null),
    number: callbackFn(a.number, b.number),
    object: callbackFn(a.object, b.object),
    string: callbackFn(a.string, b.string)
});

export const intersectJsonSetConfigs = (configA: JsonSetConfig, configB: JsonSetConfig): JsonSetConfig =>
    mapJsonSetConfigs(configA, configB, (typeSet1, typeSet2) => typeSet1.intersect(typeSet2));

export const unionJsonSetConfigs = (configA: JsonSetConfig, configB: JsonSetConfig): JsonSetConfig =>
    mapJsonSetConfigs(configA, configB, (typeSet1, typeSet2) => typeSet1.union(typeSet2));

export const complementJsonSetConfig = (config: JsonSetConfig): JsonSetConfig => ({
    array: config.array.complement(),
    boolean: config.boolean.complement(),
    integer: config.integer.complement(),
    null: config.null.complement(),
    number: config.number.complement(),
    object: config.object.complement(),
    string: config.string.complement()
});
