import {JsonSchema} from 'json-schema-spec-types';

export interface Set<T> {
    setType: T;
    type: 'all' | 'empty' | 'some';
    complement(): Set<T>;
    intersect(other: Set<T>): Set<T>;
    union(other: Set<T>): Set<T>;
    isSubsetOf(other: Set<T>): boolean;
    toJsonSchema(): JsonSchema;
}

export interface Subset<T> {
    setType: T;
    type: 'all' | 'empty' | 'some';
    complement(): Array<Subset<T>>;
    intersect(other: Subset<T>): Subset<T>;
    intersectWithSome(other: Subset<T>): Subset<T>;
    toJsonSchema(): JsonSchema;
}

export const addAnyOfIfNecessary = (schemas: JsonSchema[]): JsonSchema =>
    schemas.length === 1 ? schemas[0] : {anyOf: schemas};
