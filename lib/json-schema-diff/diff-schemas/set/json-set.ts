// tslint:disable:max-classes-per-file
import { JsonSchema } from 'json-schema-spec-types';
import {
    complementJsonSetConfig,
    intersectJsonSetConfigs,
    JsonSetConfig,
    unionJsonSetConfigs
} from './json-set/json-set-config';
import {jsonSetConfigToCoreRepresentationSchema} from './json-set/json-set-config-to-core-representation-schema';
import {Set} from './set';

interface JsonSet extends Set<'json'> {
    intersectWithSome(other: SomeJsonSet): Set<'json'>;
    unionWithSome(other: SomeJsonSet): Set<'json'>;
}

class AllJsonSet implements JsonSet {
    public readonly type = 'all';
    public readonly setType = 'json';

    public complement(): Set<'json'> {
        return emptyJsonSet;
    }

    public intersect(other: JsonSet): Set<'json'> {
        return other;
    }

    public intersectWithSome(other: SomeJsonSet): Set<'json'> {
        return other;
    }

    public union(): Set<'json'> {
        return this;
    }

    public unionWithSome(): Set<'json'> {
        return this;
    }

    public isSubsetOf(other: Set<'json'>): boolean {
        return other === this;
    }

    public toJsonSchema(): JsonSchema {
        return true;
    }
}

class EmptyJsonSet implements JsonSet {
    public readonly type = 'empty';
    public readonly setType = 'json';

    public complement(): Set<'json'> {
        return allJsonSet;
    }

    public intersect(): Set<'json'> {
        return this;
    }

    public intersectWithSome(): Set<'json'> {
        return this;
    }

    public union(other: JsonSet): Set<'json'> {
        return other;
    }

    public unionWithSome(other: JsonSet): Set<'json'> {
        return other;
    }

    public isSubsetOf(): boolean {
        return true;
    }

    public toJsonSchema(): JsonSchema {
        return false;
    }
}

class SomeJsonSet implements JsonSet {
    public readonly type = 'some';
    public readonly setType = 'json';

    public constructor(public readonly config: JsonSetConfig) {
    }

    public complement(): Set<'json'> {
        return createJsonSetFromConfig(complementJsonSetConfig(this.config));
    }

    public intersect(other: JsonSet): Set<'json'> {
        return other.intersectWithSome(this);
    }

    public intersectWithSome(other: SomeJsonSet): Set<'json'> {
        return createJsonSetFromConfig(intersectJsonSetConfigs(this.config, other.config));
    }

    public union(other: JsonSet): Set<'json'> {
        return other.unionWithSome(this);
    }

    public unionWithSome(other: SomeJsonSet): Set<'json'> {
        return createJsonSetFromConfig(unionJsonSetConfigs(this.config, other.config));
    }

    public isSubsetOf(other: Set<'json'>): boolean {
        return other.complement().intersect(this).type === 'empty';
    }

    public toJsonSchema(): JsonSchema {
        return jsonSetConfigToCoreRepresentationSchema(this.config);
    }
}

const areAllSubsetsInConfigOfType = (config: JsonSetConfig, setType: 'all' | 'empty') => {
    return Object
        .keys(config)
        .every((name: keyof JsonSetConfig) => config[name].type === setType);
};

export const allJsonSet = new AllJsonSet();
export const emptyJsonSet = new EmptyJsonSet();

export const createJsonSetFromConfig = (config: JsonSetConfig): Set<'json'> => {
    if (areAllSubsetsInConfigOfType(config, 'empty')) {
        return emptyJsonSet;
    } else if (areAllSubsetsInConfigOfType(config, 'all')) {
        return allJsonSet;
    }
    return new SomeJsonSet(config);
};
