import {JsonSchema} from 'json-schema-spec-types';
import {Subset} from '../set';
import {complementStringSubsetConfig} from './string-subset/complement-string-subset-config';
import {intersectStringSubsetConfig} from './string-subset/intersect-string-subset-config';
import {StringSubsetConfig} from './string-subset/string-subset-config';
import {stringSubsetConfigHasContradictions} from './string-subset/string-subset-config-has-contradictions';
import {stringSubsetConfigToJsonSchema} from './string-subset/string-subset-config-to-json-schema';
import {AllSubset, EmptySubset} from './subset';

class SomeStringSubset implements Subset<'string'> {
    public readonly type = 'some';
    public readonly setType = 'string';

    public constructor(private readonly config: StringSubsetConfig) {
    }

    public complement(): Array<Subset<'string'>> {
        return complementStringSubsetConfig(this.config).map(createStringSubsetFromConfig);
    }

    public intersect(other: Subset<'string'>): Subset<'string'> {
        return other.intersectWithSome(this);
    }

    public intersectWithSome(other: SomeStringSubset): Subset<'string'> {
        return createStringSubsetFromConfig(intersectStringSubsetConfig(this.config, other.config));
    }

    public toJsonSchema(): JsonSchema {
        return stringSubsetConfigToJsonSchema(this.config);
    }
}

export const allStringSubset = new AllSubset('string');
export const emptyStringSubset = new EmptySubset('string');

export const createStringSubsetFromConfig = (config: StringSubsetConfig): Subset<'string'> =>
    stringSubsetConfigHasContradictions(config)
        ? emptyStringSubset
        : new SomeStringSubset(config);
