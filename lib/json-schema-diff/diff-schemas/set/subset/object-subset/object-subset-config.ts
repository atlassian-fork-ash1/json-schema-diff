import {Set} from '../../set';

export interface ParsedPropertiesKeyword {
    [key: string]: Set<'json'>;
}

export interface ObjectSubsetConfig {
    additionalProperties: Set<'json'>;
    maxProperties: number;
    minProperties: number;
    not: ObjectSubsetConfig[];
    properties: ParsedPropertiesKeyword;
    required: string[];
}

export const getPropertyNames = (config: ObjectSubsetConfig): string[] => Object.keys(config.properties);

export const getPropertySet = (config: ObjectSubsetConfig, propertyName: string): Set<'json'> =>
    config.properties[propertyName] || config.additionalProperties;
