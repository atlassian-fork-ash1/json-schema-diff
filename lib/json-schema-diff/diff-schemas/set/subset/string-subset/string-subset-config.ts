export interface StringSubsetConfig {
    maxLength: number;
    minLength: number;
}
