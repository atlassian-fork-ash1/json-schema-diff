import {CoreSchemaMetaSchema, JsonSchema, JsonSchemaMap, SimpleTypes} from 'json-schema-spec-types';
import {
    createAllJsonSet,
    createEmptyJsonSet,
    createJsonSetFromParsedSchemaKeywords
} from './set-factories/create-json-set';
import {
    defaultMaxItems,
    defaultMaxLength,
    defaultMaxProperties,
    defaultMinItems,
    defaultMinLength,
    defaultMinProperties,
    defaultRequired,
    defaultTypes
} from './set/keyword-defaults';
import {Set} from './set/set';
import {ParsedPropertiesKeyword} from './set/subset/object-subset/object-subset-config';

const parseSchemaProperties = (schemaProperties: JsonSchemaMap = {}): ParsedPropertiesKeyword => {
    const objectSetProperties: ParsedPropertiesKeyword = {};

    for (const propertyName of Object.keys(schemaProperties)) {
        const propertySchema = schemaProperties[propertyName];
        objectSetProperties[propertyName] = parseSchemaOrUndefinedAsJsonSet(propertySchema);
    }
    return objectSetProperties;
};

const parseType = (type: SimpleTypes | SimpleTypes[] | undefined): SimpleTypes[] => {
    if (!type) {
        return defaultTypes;
    }

    if (typeof type === 'string') {
        return [type];
    }

    return type;
};

const parseRequiredKeyword = (required: string[] | undefined): string[] => required || defaultRequired;

const parseNumericKeyword = (keywordValue: number | undefined, defaultValue: number): number =>
    typeof keywordValue === 'number' ? keywordValue : defaultValue;

const parseTypeKeywords = (schema: CoreSchemaMetaSchema): Set<'json'> =>
    createJsonSetFromParsedSchemaKeywords({
        additionalProperties: parseSchemaOrUndefinedAsJsonSet(schema.additionalProperties),
        items: parseSchemaOrUndefinedAsJsonSet(schema.items),
        maxItems: parseNumericKeyword(schema.maxItems, defaultMaxItems),
        maxLength: parseNumericKeyword(schema.maxLength, defaultMaxLength),
        maxProperties: parseNumericKeyword(schema.maxProperties, defaultMaxProperties),
        minItems: parseNumericKeyword(schema.minItems, defaultMinItems),
        minLength: parseNumericKeyword(schema.minLength, defaultMinLength),
        minProperties: parseNumericKeyword(schema.minProperties, defaultMinProperties),
        properties: parseSchemaProperties(schema.properties),
        required: parseRequiredKeyword(schema.required),
        type: parseType(schema.type)
    });

const parseAllOfKeyword = (allOfSchemas: JsonSchema[] | undefined): Array<Set<'json'>> =>
    (allOfSchemas || []).map(parseSchemaOrUndefinedAsJsonSet);

const parseAnyOfKeyword = (anyOfSchemas: JsonSchema[] | undefined): Array<Set<'json'>> => {
    if (!anyOfSchemas) {
        return [];
    }

    const parsedAnyOfSet = anyOfSchemas
        .map(parseSchemaOrUndefinedAsJsonSet)
        .reduce((accumulator, set) => accumulator.union(set));
    return [parsedAnyOfSet];
};

const parseNotKeyword = (notSchema: JsonSchema | undefined): Array<Set<'json'>> => {
    if (!notSchema) {
        return [];
    }

    return [parseSchemaOrUndefinedAsJsonSet(notSchema).complement()];
};

const parseOneOfKeyword = (oneOfSchemas: JsonSchema[] | undefined): Array<Set<'json'>> => {
    if (!oneOfSchemas) {
        return [];
    }

    // oneOf: [A, B, C] = (A && !B && !C) || (!A && B && !C) || (!A && !B && C)

    const parsedSchemaList = oneOfSchemas.map(parseSchemaOrUndefinedAsJsonSet);
    const complementedSchemas = parsedSchemaList.map((schema) => schema.complement());
    const parsedOneOfSchema = parsedSchemaList
        .map((schema, i) => [...complementedSchemas.slice(0, i), schema, ...complementedSchemas.slice(i + 1)])
        .map((schemaGroup) => schemaGroup.reduce((acc, schema) => acc.intersect(schema)))
        .reduce((acc, schemaGroup) => acc.union(schemaGroup));

    return [parsedOneOfSchema];
};

const parseBooleanLogicKeywords = (schema: CoreSchemaMetaSchema): Array<Set<'json'>> =>
    [
        ...parseAnyOfKeyword(schema.anyOf),
        ...parseAllOfKeyword(schema.allOf),
        ...parseNotKeyword(schema.not),
        ...parseOneOfKeyword(schema.oneOf)
    ];

const parseCoreSchemaMetaSchema = (schema: CoreSchemaMetaSchema): Set<'json'> => {
    const typeKeywordsSet = parseTypeKeywords(schema);
    const booleanLogicKeywordSets = parseBooleanLogicKeywords(schema);
    return booleanLogicKeywordSets.reduce((accumulator, set) => accumulator.intersect(set), typeKeywordsSet);
};

const parseBooleanSchema = (schema: boolean | undefined): Set<'json'> => {
    const allowsAllJsonValues = schema === undefined ? true : schema;
    return allowsAllJsonValues ? createAllJsonSet() : createEmptyJsonSet();
};

const parseSchemaOrUndefinedAsJsonSet = (schema: JsonSchema | undefined): Set<'json'> => {
    return (typeof schema === 'boolean' || schema === undefined)
        ? parseBooleanSchema(schema)
        : parseCoreSchemaMetaSchema(schema);
};

export const parseAsJsonSet = (schema: JsonSchema): Set<'json'> => {
    return parseSchemaOrUndefinedAsJsonSet(schema);
};
