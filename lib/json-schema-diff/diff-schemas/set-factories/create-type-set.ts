import {SimpleTypes} from 'json-schema-spec-types';
import {Set, Subset} from '../set/set';
import {AllSubset, EmptySubset} from '../set/subset/subset';
import {createTypeSetFromSubsets} from '../set/type-set';
import {isTypeSupported} from './is-type-supported';

const createTypeSubset = <T extends SimpleTypes> (setType: T, types: SimpleTypes[]): Subset<T> =>
    isTypeSupported(types, setType) ? new AllSubset(setType) : new EmptySubset(setType);

export const createTypeSet = <T extends SimpleTypes>(setType: T, types: SimpleTypes[]): Set<T> =>
    createTypeSetFromSubsets(setType, [createTypeSubset(setType, types)]);
