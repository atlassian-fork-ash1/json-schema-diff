"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const keyword_defaults_1 = require("../set/keyword-defaults");
const object_subset_1 = require("../set/subset/object-subset");
const type_set_1 = require("../set/type-set");
const is_type_supported_1 = require("./is-type-supported");
const supportsAllObjects = (objectSetParsedKeywords) => {
    // tslint:disable:cyclomatic-complexity
    const everyPropertyIsAll = Object.keys(objectSetParsedKeywords.properties)
        .every((propertyName) => objectSetParsedKeywords.properties[propertyName].type === 'all');
    return everyPropertyIsAll
        && _.isEqual(objectSetParsedKeywords.required, keyword_defaults_1.defaultRequired)
        && objectSetParsedKeywords.additionalProperties.type === 'all'
        && objectSetParsedKeywords.minProperties === keyword_defaults_1.defaultMinProperties
        && objectSetParsedKeywords.maxProperties === keyword_defaults_1.defaultMaxProperties;
};
const createObjectSubset = (objectSetParsedKeywords) => {
    if (!is_type_supported_1.isTypeSupported(objectSetParsedKeywords.type, 'object')) {
        return object_subset_1.emptyObjectSubset;
    }
    if (supportsAllObjects(objectSetParsedKeywords)) {
        return object_subset_1.allObjectSubset;
    }
    return object_subset_1.createObjectSubsetFromConfig(Object.assign({}, objectSetParsedKeywords, { not: [] }));
};
exports.createObjectSet = (objectSetParsedKeywords) => {
    const objectSubset = createObjectSubset(objectSetParsedKeywords);
    return type_set_1.createTypeSetFromSubsets('object', [objectSubset]);
};
