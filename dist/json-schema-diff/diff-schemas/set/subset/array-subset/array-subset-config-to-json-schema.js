"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const keyword_defaults_1 = require("../../keyword-defaults");
const set_1 = require("../../set");
const getNotSchema = (config) => config.not.length === 0 ? {} : { not: set_1.addAnyOfIfNecessary(config.not.map(exports.arraySubsetConfigToJsonSchema)) };
const getItemsSchema = (config) => config.items.type === 'all' ? {} : { items: config.items.toJsonSchema() };
const getMaxItemsSchema = (config) => config.maxItems === keyword_defaults_1.defaultMaxItems ? {} : { maxItems: config.maxItems };
const getMinItemsSchema = (config) => config.minItems === keyword_defaults_1.defaultMinItems ? {} : { minItems: config.minItems };
exports.arraySubsetConfigToJsonSchema = (config) => (Object.assign({ type: 'array' }, getItemsSchema(config), getMaxItemsSchema(config), getMinItemsSchema(config), getNotSchema(config)));
